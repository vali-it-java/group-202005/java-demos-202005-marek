package ee.bcs.valiit.week4;

import java.util.ArrayList;
import java.util.List;

public class TestVariant1Solutions {
    public static void main(String[] args) {

        System.out.println("Ex 4");
        List<Country> countries = new ArrayList<>();
        countries.add(new Country("Switzerland", 83716));
        countries.add(new Country("Singapore", 63987));
        countries.add(new Country("Ireland", 77771));
        countries.add(new Country("Iceland", 67037));
        countries.add(new Country("Macau", 81151));
        countries.add(new Country("Denmark", 59795));
        countries.add(new Country("Qatar", 69687));
        countries.add(new Country("United States", 65111));
        countries.add(new Country("Norway", 77975));
        countries.sort((c1, c2) -> c2.getGdp() - c1.getGdp());
        System.out.println(countries);
    }

    public static class Country {
        private String name;
        private int gdp;

        public Country(String name, int gdp) {
            this.name = name;
            this.gdp = gdp;
        }

        public String getName() {
            return name;
        }

        public int getGdp() {
            return gdp;
        }

        @Override
        public String toString() {
            return "Country{" +
                    "name='" + name + '\'' +
                    ", gdp=" + gdp +
                    '}';
        }
    }
}
