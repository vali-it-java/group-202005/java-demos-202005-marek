package ee.bcs.valiit.week4;

import java.math.BigDecimal;
import java.util.Arrays;

public class Test1 {

    public static void main(String[] args) {

        double eulerConstant = Math.E * 2;
        BigDecimal eulerConstant2 = new BigDecimal("0.577215664901532860606512090082402431042159335");
        eulerConstant2 = eulerConstant2.multiply(new BigDecimal("2"));
        System.out.println(eulerConstant2);

        int area = calcRectangleArea(5, 6);
        System.out.println("Pindala: " + area);

        String[] words = {"auto", "rebane", "sau", "saabas"};
        for(String w : censorWords(words)) {
            System.out.println(w);
        }

        String[] genres = {"Punk", "Pop"};
        Song song = new Song(
                "Täna jälle me joome bensiini",
                "Vennaskond",
                "Inglid ja kangelased",
                1995,
                genres,
                "Maha põlegu Tallinn ja Riia\n" +
                "Ja las leekides hukkub Berliin.\n" +
                "Täna jälle on meelitand siia\n" +
                "Uimas tüdrukud mind ja bensiin\n" +
                "Täna jälle ei ole siin kohti,\n" +
                "Kus vaid sädemest ei tõuseks leek.\n" +
                "Täna jälle on siin tuleohtlik.\n" +
                "Täna jälle me suitsu ei tee\n" +
                "Täna jälle me joome bensiini.\n" +
                "Täna jälle mul voolik on suus.\n" +
                "Täna jälle kanister käib ringi.\n" +
                "Täna jälle A 76.\n" +
                "Taas kui horisont kumab siis kaagid\n" +
                "ära lähevad päev koidab uus.\n" +
                "Lähen minagi tühjad on paagid.\n" +
                "Läbi sudu bensiinimaik suus.");
        System.out.println(song);
    }

    private static int calcRectangleArea(int sideA, int sideB) {
        return sideA * sideB;
    }

    private static String[] censorWords(String[] words) {
        String[] result = new String[words.length];

        // Must maagia...
        for (int i = 0; i < words.length; i++) {
//            result[i] = "#".repeat(words[i].length());
            result[i] = "";
            for (int j = 0; j < words[i].length(); j++) {
                result[i] = result[i] + "#";
            }
        }

        return result;
    }

    public static class Song {
        private String title;
        private String artist;
        private String album;
        private int released;
        private String[] genres;
        private String lyrics;

        public Song(String title, String artist, String album, int released, String[] genres, String lyrics) {
            this.title = title;
            this.artist = artist;
            this.album = album;
            this.released = released;
            this.genres = genres;
            this.lyrics = lyrics;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

        public String getAlbum() {
            return album;
        }

        public void setAlbum(String album) {
            this.album = album;
        }

        public int getReleased() {
            return released;
        }

        public void setReleased(int released) {
            this.released = released;
        }

        public String[] getGenres() {
            return genres;
        }

        public void setGenres(String[] genres) {
            this.genres = genres;
        }

        public String getLyrics() {
            return lyrics;
        }

        public void setLyrics(String lyrics) {
            this.lyrics = lyrics;
        }

        @Override
        public String toString() {
            return "Song{" +
                    "title='" + title + '\'' +
                    ", artist='" + artist + '\'' +
                    ", album='" + album + '\'' +
                    ", released=" + released +
                    ", genres=" + Arrays.toString(genres) +
                    ", lyrics='" + lyrics + '\'' +
                    '}';
        }
    }
}
