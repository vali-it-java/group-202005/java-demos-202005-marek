package ee.bcs.valiit.week4;

import java.util.Arrays;
import java.util.List;

public class TestVariant2Solutions {
    public static void main(String[] args) {

        System.out.println("Ex 2");
        System.out.println(calculateAverageIncomeTax(new int[]{1700, 1500, 2320, 980, 1320, 1500, 1550, 1790}));

        System.out.println("Ex 3");
        System.out.println(censorMs("Meelas mõmm maiustas mäe otsas maasikatega."));
        System.out.println(censorMs("Kuri karu järas jõe ääres juurikaid."));

        System.out.println("Ex 4");
        List<Country> countries = Arrays.asList(
                new Country("Switzerland", 83716, 8654622),
                new Country("Singapore", 63987, 5850342),
                new Country("Ireland", 77771, 4937786),
                new Country("Iceland", 67037, 341243),
                new Country("Macau", 81151, 649335),
                new Country("Denmark", 59795, 5792202),
                new Country("Qatar", 69687, 2881053),
                new Country("United States", 65111, 331002651),
                new Country("Norway", 77975, 5421241)
        );

        Country biggestEconomy = findBiggestEconomy(countries);
        System.out.println("Suurim majandus:");
        System.out.println(biggestEconomy);

//        Country smallestEconomy = countries.stream().min((c1, c2) -> {
//            if ( c1.getTotalGdp() - c2.getTotalGdp() > 0 ) {
//                return 1;
//            } else if (c1.getTotalGdp() - c2.getTotalGdp() < 0) {
//                return -1;
//            } else {
//                return 0;
//            }
//        }).get();

        Country smallestEconomy = countries.stream()
                .min((c1, c2) -> Double.compare(c1.getTotalGdp(), c2.getTotalGdp())).get();
        System.out.println("Väikseim majandus:");
        System.out.println(smallestEconomy);

        double averageEconomy = countries.stream().mapToDouble(c -> c.getTotalGdp()).average().orElse(0);
        System.out.printf("Keskmine majandus: %,.2f USD.", averageEconomy);
    }

    private static double calculateAverageIncomeTax(int[] salaries) {
        double totalSalary = 0;
        for (int salary : salaries) {
            totalSalary += salary;
        }
        return totalSalary * 0.2 / salaries.length;
    }

    private static String censorMs(String text) {
        if (text != null) {
            return text.replace("m", "#").replace("M", "#");
        }
        return null;
    }

    private static Country findBiggestEconomy(List<Country> countries) {
        Country biggestEconomy = null;
        for (Country country : countries) {
            if (biggestEconomy == null) {
                biggestEconomy = country;
            } else if (country.getTotalGdp() > biggestEconomy.getTotalGdp()) {
                biggestEconomy = country;
            }
        }
        return biggestEconomy;
    }

    public static class Country {
        private String name;
        private double gdpPerCapita;
        private double population;

        public Country(String name, double gdpPerCapita, double population) {
            this.name = name;
            this.gdpPerCapita = gdpPerCapita;
            this.population = population;
        }

        public String getName() {
            return name;
        }

        public double getGdpPerCapita() {
            return gdpPerCapita;
        }

        public double getPopulation() {
            return population;
        }

        public double getTotalGdp() {
            return population * gdpPerCapita;
        }

        @Override
        public String toString() {
            return "Country{" +
                    "name='" + name + '\'' +
                    ", gdpPerCapita=" + gdpPerCapita +
                    ", population=" + population +
                    ", totalGdp=" + String.format("%,.2f", this.getTotalGdp()) +
                    '}';
        }
    }
}
