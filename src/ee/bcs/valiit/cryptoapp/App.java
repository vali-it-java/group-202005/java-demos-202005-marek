package ee.bcs.valiit.cryptoapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws IOException {
        System.out.println("-----------------------------------");
        System.out.println("-     VALI IT SPIOONIRAKENDUS     -");
        System.out.println("-----------------------------------");

        Scanner scanner = new Scanner(System.in);
        Cryptor encryptor = null; // = new Encryptor("resources/alfabeet.txt");
        Cryptor decryptor = null; //= new Decryptor("resources/alfabeet.txt");

        while(true) {
            System.out.print("Mida soovid teha? [1 - krüpteeri, 2 - dekrüpteeri, 3 - välju programmist]: ");
            String input = scanner.nextLine();
            switch (input) {
                case "1":
                    System.out.println("1");
                    if (encryptor == null) {
                        encryptor = new Encryptor("resources/alfabeet.txt");
                    }
                    System.out.println("Sisesta tekst:");
                    String message = scanner.nextLine().toUpperCase();
                    System.out.println("Krüpteeritud tekst:");
                    System.out.println(encryptor.convert(message));
                    break;
                case "2":
                    System.out.println("2");
                    if (decryptor == null) {
                        decryptor = new Decryptor("resources/alfabeet.txt");
                    }
                    System.out.println("Sisesta krüpteeritud tekst:");
                    String encryptedMessage = scanner.nextLine().toUpperCase();
                    System.out.println("Krüpteeritud tekst:");
                    System.out.println(decryptor.convert(encryptedMessage));
                    break;
                case "3":
                    System.out.println("3");
                    System.out.println("Aitäh kasutamast!");
                    System.out.println("Ööd on siin mustad!");
                    return;
                default:
                    System.out.println("Ei saanud aru :(");
            }
        }

//        String message = args[0];
//        Cryptor cryptor = new Encryptor("resources/alfabeet.txt");
////        Encryptor cryptor = new SuperEncryptor("resources/asdfasd.txt");
//        String encryptedMessage = cryptor.convert(message); // Tulemuseks on krüpteeritud sõnum.
//        System.out.println("Krüpteeritud sõnum: " + encryptedMessage);
//
////        Cryptor decryptor = new SuperDecryptor("asdfasdfasdfd");
//        Cryptor decryptor = new Decryptor("resources/alfabeet.txt");
//        String decryptedMessage = decryptor.convert(encryptedMessage);
//        System.out.println("Dekrüpteeritud sõnum: " + decryptedMessage);
    }
}
