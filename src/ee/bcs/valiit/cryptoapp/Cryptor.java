package ee.bcs.valiit.cryptoapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {
    private final Map<String, String> alphabetMap;

    public Cryptor(String alphabetFilePath) throws IOException {
        List<String> alphabetLines = this.getAlphabetLines(alphabetFilePath);
        this.alphabetMap = this.getAlphabetMap(alphabetLines);
    }

    protected abstract Map<String, String> getAlphabetMap(List<String> alphabetLines);

    public String convert(String message) {
        String convertedMessage = "";
        for (String c : message.split("")) {
            String encryptedChar = alphabetMap.containsKey(c.toUpperCase()) ?
                    alphabetMap.get(c.toUpperCase()) : c.toUpperCase();
            convertedMessage += encryptedChar;
        }
        return convertedMessage;
    }

    private List<String> getAlphabetLines(String alphabetFilePath) throws IOException {
        return Files.readAllLines(Paths.get(alphabetFilePath));
    }
}
