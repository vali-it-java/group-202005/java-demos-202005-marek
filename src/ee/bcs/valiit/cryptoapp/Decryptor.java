package ee.bcs.valiit.cryptoapp;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Decryptor extends Cryptor {

    public Decryptor(String alphabetFilePath) throws IOException {
        super(alphabetFilePath);
    }

    @Override
    protected Map<String, String> getAlphabetMap(List<String> alphabetLines) {
        // List: "M, R" | Map: "R" -> "M"
        Map<String, String> alphabetMap = new HashMap<>();
        for (String line : alphabetLines) {
            String[] lineParts = line.split(", ");
            alphabetMap.put(lineParts[1], lineParts[0]);
        }
        return alphabetMap;
    }
}
