package ee.bcs.valiit.bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccountService {

    private static List<Account> accounts = new ArrayList<>();

    public static List<Account> getAccounts() {
        return accounts;
    }

    public static void loadAccounts(String accountFilePath) throws IOException {
        List<String> accountCsvs = Files.readAllLines(Paths.get(accountFilePath));

        // Ma tahan teha seda iga reaga kontode failist...
        // "Kimberly, Carson, 321811857, 657" -->
        // firstName: "Kimberly", lastName: "Carson", accountNumber: "321811857", balance: 657.0

        // Tekitame konto objektid ja paneme need listi.
//        for (String accountCsv : accountCsvs) {
//            Account acc = new Account(accountCsv);
//            accounts.add(acc);
//        }
        accounts = accountCsvs.stream().map(Account::new).collect(Collectors.toList());
    }

    public static Account findAccount(String accountNumber) {
        // Otsime accounts listist sobiva account objekti ja tagastame selle...
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }
        }
        return null;

//        return accounts.stream()
//                .filter(a -> a.getAccountNumber().equals(accountNumber)).findFirst().orElse(null);

//        List<Account> filteredAccounts = accounts
//                .stream().filter(acc -> acc.getAccountNumber().equals(accountNumber))
//                .collect(Collectors.toList());
//        return filteredAccounts.size() > 0 ? filteredAccounts.get(0) : null;
    }

    public static Account findAccount(String firstName, String lastName) {
        for (Account account : accounts) {
            if (account.getFirstName().equalsIgnoreCase(firstName) &&
                    account.getLastName().equalsIgnoreCase(lastName)) {
                return account;
            }
        }
        return null;

//        return accounts.stream()
//                .filter(account -> account.getFirstName().equalsIgnoreCase(firstName) &&
//                        account.getLastName().equalsIgnoreCase(lastName))
//                .findFirst().orElse(null);
    }

    public static TransferResult transfer(String fromAccountNumber, String toAccountNumber, double sum) {
        Account from = findAccount(fromAccountNumber);
        Account to = findAccount(toAccountNumber);

        // Valideerimine (Kas saab üldse ülekannet teha?)...
        if (from == null) {
            return new TransferResult(false, "Maksja kontot ei eksisteeri.");
        } else if (to == null) {
            return new TransferResult(false, "Saaja kontot ei eksisteeri.");
        } else if (!(sum > 0)) {
            return new TransferResult(false, "Ülekantav summa peab olema positiivne.");
        } else if (from.getBalance() < sum) {
            return new TransferResult(false, "Vahendid puuduvad.");
        }

        // Happy case algab siit...
        from.setBalance(from.getBalance() - sum);
        to.setBalance(to.getBalance() + sum);
        return new TransferResult(true, "OK", from, to);
    }
}
