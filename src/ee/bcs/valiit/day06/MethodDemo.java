package ee.bcs.valiit.day06;

public class MethodDemo {

    public static void main(String[] args) {

        // Funktsiooni väljakutsumine ehk käivitamine
//        double randomNumber = Math.random();
        int myCalculatedResult = (doSomePointlessCalculation(11, 2, 3) + 2) / 3;
        System.out.println(myCalculatedResult);
        printGreetingToConsole();
        printAdvancedGreeting("Marek");
    }

    // Funktsiooni defineerimine
    private static int doSomePointlessCalculation(int number1, int number2, int number3) {
        int result = (number1 + number2 + number3) / 2;
        return result; // Väärtus tagastatakse
    }

    private static void printGreetingToConsole() {
        System.out.println("Hello World!");
    }

    private static void printAdvancedGreeting(final String name) {
        // Valideerimine
        if (name.equals("Matthew")) {
            return; // Mathew'd ei tervitata.
        }
        System.out.println("Hello " + name + "!");
    }
}
