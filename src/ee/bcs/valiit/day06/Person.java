package ee.bcs.valiit.day06;

public class Person {
    public String personalCode;

    public Person () {

    }

    public Person(String personalCode) {
        System.out.println("Konstruktor käivitus...");
        this.personalCode = personalCode;
    }

    public String getGender() {
        int firstDigit = Integer.parseInt(this.personalCode.substring(0, 1));
        return firstDigit % 2 == 1 ? "M" : "F";
    }

    public int getBirthYear() {
        if (this.personalCode == null) {
            return -1;
        } else if (this.personalCode.length() != 11) {
            return -1;
        }

        int century = Integer.parseInt(this.personalCode.substring(0, 1));
        int birthYear = Integer.parseInt(this.personalCode.substring(1, 3));

        switch (century) {
            case 1:
            case 2:
                return 1800 + birthYear;
            case 3:
            case 4:
                return 1900 + birthYear;
            case 5:
            case 6:
                return 2000 + birthYear;
            case 7:
            case 8:
                return 2100 + birthYear;
            default:
                return -1;
        }
    }

    public String getBirthMonth() {
        int birthMonth = Integer.parseInt(this.personalCode.substring(3, 5));
        switch (birthMonth) {
            case 1:
                return "Jaanuar";
            case 2:
                return "Veebruar";
            case 3:
                return "Märts";
            case 4:
                return "Aprill";
            case 5:
                return "Mai";
            case 6:
                return "Juuni";
            case 7:
                return "Juuli";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "Oktoober";
            case 11:
                return "November";
            case 12:
                return "Detsember";
            default:
                return "N/A";
        }
    }

    public int getDayOfMonth() {
        return Integer.parseInt(this.personalCode.substring(5, 7));
    }
}
