package ee.bcs.valiit.day06;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodExercises {
    public static void main(String[] args) {

        System.out.println("Ex 1");
        test(3);

        System.out.println("Ex 2");
        test2("tere", "head aega");

        System.out.println("Ex 3");
        System.out.println(addVat(89.0));

        System.out.println("Ex 4");
        int[] myEx4Array = composeArray(4, 5, false);
        System.out.println(Arrays.toString(myEx4Array));

        System.out.println("Ex 5");
        printTere();

        System.out.println("Ex 6");
        String gender = deriveGender("49403136526");
        System.out.println(gender);

//        String personalCode1 = "49403136526";
        String personalCode1 = "34501234215";
        int birthYear1 = retrieveBirthYear(personalCode1);
        System.out.printf("Isikukood: %s, sünniaasta: %s\n", personalCode1, birthYear1);

        System.out.println("Ex 8");
        System.out.printf("Kas isikukood: %s on korrektne? %s\n", personalCode1, validatePersonalCode​(personalCode1));

//        validateSomething();
        MethodExercises myMethodExercisesObject = new MethodExercises();
        myMethodExercisesObject.validateSomething();

        System.out.println("Rekursioon");
        printNumbersDesc(100);

        // 3! = 1 * 2 * 3 = 6
        System.out.println(factorial(3));

        System.out.println(true);
        System.out.println(2);
        System.out.println(5.6);
        System.out.println('r');
        System.out.println("tere");
        System.out.println();

//        aFunction("2");
//        aFunction(456);
//        aFunction(true);
    }

//    public static void aFunction(Object input) {
//        System.out.println(input);
//    }

    private static boolean test(int a) {
        return true;
    }

    private static void test2(String text1, String text2) {
    }

    private static double addVat(double netPrice) {
        return 1.2 * netPrice;
    }

    private static int[] composeArray(int number1, int number2, boolean duplicate) {
        if (duplicate) {
            return new int[]{number1 * 2, number2 * 2};
        } else {
            return new int[]{number1, number2};
        }
    }

    private static void printTere() {
        System.out.println("Tere");
    }

    private static String deriveGender(String personalCode) { // "49403136526" --> "F"
        int firstDigit = Integer.parseInt(personalCode.substring(0, 1));
        return firstDigit % 2 == 1 ? "M" : "F";
//        String gender;
//        if (firstDigit % 2 == 1) {
//            gender = "M";
//        } else {
//            gender = "F";
//        }
//        return gender;
    }

    public static int retrieveBirthYear(String personalCode) {  // "49403136526"
        // .substring()
        // Integer.parseInt()

        // Valideerimine
        if (personalCode == null) {
            return -1;
        } else if (personalCode.length() != 11) {
            return -1;
        }

        int century = Integer.parseInt(personalCode.substring(0, 1)); // 4
        int birthYear = Integer.parseInt(personalCode.substring(1, 3)); // 94

        switch (century) {
            case 1:
            case 2:
                return 1800 + birthYear;
            case 3:
            case 4:
                return 1900 + birthYear;
            case 5:
            case 6:
                return 2000 + birthYear;
            case 7:
            case 8:
                return 2100 + birthYear;
            default:
                return -1;
        }
    }

    private static boolean validatePersonalCode​(String personalCode) { // "49403136526" --> true
        if (personalCode == null) {
            return false;
        } else if (personalCode.length() != 11) {
            return false;
        }

        String[] personalCodeParts = personalCode.split("");
        // Tulemus: String[11] { "4", "9", "4", "0", "3", "1", "3", "6", "5", "2", "6" }

        int[] personalCodeDigits = new int[11];
        for (int i = 0; i < 11; i++) {
            personalCodeDigits[i] = Integer.parseInt(personalCodeParts[i]);
        }

//      Stream näited...
//        int[] digits = Stream.of(personalCode.split("")).mapToInt(Integer::parseInt).toArray();
//        int[] numbers = Arrays.stream(personalCode.split("")).mapToInt(Integer::parseInt).toArray();
//        Integer[] digits2 = Stream.of(personalCode.split(""))
//                .map(s -> Integer.parseInt(s))
//                .collect(Collectors.toList())
//                .toArray(Integer[]::new);

        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int sum = 0;

        // Summeerime kokku kaalu ja isikukoodi vastavate numbrite korrutised...
        // sum = 1 * 4 + 2 * 9 + ...
        for (int i = 0; i < 10; i++) {
            sum = sum + weights1[i] * personalCodeDigits[i];
        }

        int checkNumber = sum % 11;

        if (checkNumber != 10) {
            // Tagastame kas true või false.
            return personalCodeDigits[10] == checkNumber;
        }

        int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
        sum = 0;
        for (int i = 0; i < 10; i++) {
            sum = sum + weights2[i] * personalCodeDigits[i];
        }

        checkNumber = sum % 11;

        if (checkNumber != 10) {
            return personalCodeDigits[10] == checkNumber;
        } else {
            return personalCodeDigits[10] == 0;
        }
    }

    // See meetod ei kuulu klassile
    public void validateSomething() {

    }

    public static void printNumbersDesc(int number) {
        System.out.println(number);
        if (number > 1) {
            printNumbersDesc(--number);
        }
    }

    public static int factorial(int x) {
        return x <= 1 ? x : factorial(x-1) * x;
    }
}
