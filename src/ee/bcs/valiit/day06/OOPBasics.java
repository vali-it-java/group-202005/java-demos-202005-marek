package ee.bcs.valiit.day06;

import ee.bcs.valiit.pranglija.GameResult;

public class OOPBasics {
    public static void main(String[] args) {

        // new operaator on paljundusmasin.
        Pet pet1 = new Pet();
        pet1.name = "Muki";
        pet1.introducde();
//        Pet.introduce(pet1);

        Pet pet2 = new Pet();
        pet2.name = "Rex";
        pet2.introducde();

        System.out.println("Hakkan looma objekti Person klassist...");
        Person person1 = new Person("49403136526");
//        Person person1 = new Person();
        System.out.println("Objekt loodud!");
        person1.personalCode = "49403136526";
        System.out.println(person1.getGender());
        System.out.println(person1.getBirthYear());
        System.out.println(person1.getBirthMonth());
        System.out.println(person1.getDayOfMonth());

//        GameResult gameResult = new GameResult();


    }
}
