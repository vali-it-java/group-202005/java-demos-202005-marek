package ee.bcs.valiit.day06;

import java.util.Arrays;

public class CountryApp {
    public static void main(String[] args) {

        CountryInfo estonia = new CountryInfo("Eesti", "Tallinn", "Jüri Ratas",
                new String[]{"Eesti", "Vene", "Soome", "Läti", "Võru"});

        CountryInfo latvia = new CountryInfo("Läti", "Riia", "Arturs Krišjānis Kariņš",
                new String[]{"Läti", "Vene", "Leedu"});

        CountryInfo lithuania = new CountryInfo();
        lithuania.countryName = "Leedu";
        lithuania.capital = "Vilnius";
        lithuania.primeMinister = "Saulius Skvernelis";
        lithuania.languages = new String[]{"Leedu", "Poola", "Saksa", "Vene"};

        CountryInfo[] balticStates = new CountryInfo[3];
        balticStates[0] = estonia;
        balticStates[1] = latvia;
        balticStates[2] = lithuania;

        System.out.println(Arrays.toString(balticStates));
        System.out.println("Loodud riikide arv: " + CountryInfo.countryCount);
    }
}
