package ee.bcs.valiit.day06;

import java.util.Arrays;

public class CountryInfo {
    public String countryName;
    public String capital;
    public String primeMinister;
    public String[] languages;

    public static int countryCount = 0;

    public CountryInfo() {
        CountryInfo.countryCount++;
    }

    public CountryInfo(String countryName, String capital, String primeMinister, String[] languages) {
        this.countryName = countryName;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
        CountryInfo.countryCount++;
    }

    @Override
    public String toString() {
        return "{" +
                "'" + countryName + '\'' +
                ", '" + capital + '\'' +
                ", '" + primeMinister + '\'' +
                ", " + Arrays.toString(languages) +
                '}';
    }
}
