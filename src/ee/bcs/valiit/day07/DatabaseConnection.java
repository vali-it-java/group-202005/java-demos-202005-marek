package ee.bcs.valiit.day07;

// Singelton patterni näide

public class DatabaseConnection {
    private static DatabaseConnection connection = null;

    private DatabaseConnection(){

    }

    public static DatabaseConnection getDbConnection() {
        if (connection == null) {
            connection = new DatabaseConnection();
        }
        return connection;
    }
}
