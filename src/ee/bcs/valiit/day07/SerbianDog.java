package ee.bcs.valiit.day07;

public class SerbianDog extends Dog {

    public SerbianDog(String name) {
        super(name);
    }

    @Override
    public void bark() {
        System.out.println("av-av");
    }
}
