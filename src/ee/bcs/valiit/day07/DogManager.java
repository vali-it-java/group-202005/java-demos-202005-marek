package ee.bcs.valiit.day07;

public class DogManager {
    public static void main(String[] args) {
        System.out.println("Greek dog");
        Dog greekDog = new GreekDog("Aphrodite");
        System.out.println(greekDog);
        greekDog.bark();

        System.out.println("Serbian dog");
        Dog serbianDog = new SerbianDog("Šarko");
        System.out.println(serbianDog);
        serbianDog.bark();

        System.out.println("Latvian dog");
        Dog latvianDog = new LatvianDog("Rufo");
        System.out.println(latvianDog);
        latvianDog.bark();
    }
}
