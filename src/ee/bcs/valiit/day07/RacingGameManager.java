package ee.bcs.valiit.day07;

public class RacingGameManager {
    public static void main(String[] args) {

        Movable ferrari = new FerrariF355();
        Movable renault = new RenaultMegane();

        System.out.println("Ferrari: " + ferrari.getManufacturer());
        System.out.println("Renault: " + renault.getMaxSpeed());

        ferrari.speedUp(234);
        renault.slowDown(34);
    }
}
