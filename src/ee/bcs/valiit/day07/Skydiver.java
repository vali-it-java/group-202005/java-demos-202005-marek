package ee.bcs.valiit.day07;

public class Skydiver extends Athlete {

    public Skydiver(String firstName, String lastName, int age, String gender, double height, double weight) {
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public void perform() {
        System.out.println("Falling from sky...");
    }
}
