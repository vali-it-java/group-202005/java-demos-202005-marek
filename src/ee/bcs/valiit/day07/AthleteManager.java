package ee.bcs.valiit.day07;

import ee.bcs.valiit.pranglija.GameResult;

import java.util.*;

public class AthleteManager {
    public static void main(String[] args) {
        // instantsialiseerimine == objekti loomine
//        Athlete athlete1 = new Athlete(); // Vaatleme seda kui athleeti
//        Object athlete2 = new Athlete(); // Vaatleme seda kui objekti

        // Polümorfism
//        Object skydiver1 = new Skydiver();
//        Athlete skydiver2 = new Skydiver();
//        Skydiver skydiver3 = new Skydiver();

//        skydiver2.perform();

//        List<String> list = new ArrayList<>();
//        Set<String> set = new TreeSet<>();

        Athlete skydiver = new Skydiver("Tanel", "Padar", 35, "M", 187, 85);
        Athlete runner = new Runner("Heidi", "Purga", 40, "F", 168, 65, 35);

        skydiver.perform();
        runner.perform();

        System.out.println(skydiver.getFirstName());

//        GameResult result = new GameResult();
//
//        class SuperGameResult extends GameResult {
//
//            public void printName() {
//                this.name
//            }
//        }
//
//        SuperGameResult superResult = new SuperGameResult();
//        superResult.printName();

        Human h1 = new Human();
        h1.setFirstName("Maarja");
        h1.setFirstName("Maarja");
        h1.setFirstName("Maarja");
        System.out.println(h1.getFirstName());

    }
}
