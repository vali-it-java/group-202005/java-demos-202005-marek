package ee.bcs.valiit.day07;

// Interface (liides) on alati abstraktne
public interface Movable {

    String getManufacturer();
    double getMaxSpeed();
    int getProductionYear();
    void speedUp(double finalSpeed);
    void slowDown(double finalSpeed);


}
