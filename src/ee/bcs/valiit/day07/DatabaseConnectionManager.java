package ee.bcs.valiit.day07;

public class DatabaseConnectionManager {
    public static void main(String[] args) {

        DatabaseConnection connection1 = DatabaseConnection.getDbConnection();
        DatabaseConnection connection2 = DatabaseConnection.getDbConnection();
        DatabaseConnection connection3 = DatabaseConnection.getDbConnection();
    }
}
