package ee.bcs.valiit.day02;

public class TextProcessing {

    public static void main(String[] args) {

        // Stringide defineerimine
        String text1 = "Tere!";
        String text2 = new String("Tere!");

        // Stringide kokkuliitmine
        String text3 = text1 + text2;
        System.out.println("Text3: " + text3);
        String text4 = text1.concat("TERE!");
        System.out.println("Text4: " + text4);
//        text4 = text3;
//        System.out.println(text3);
//        text4 = "abc"; // String on immutable (see tähendab: muutumatu)
//        System.out.println(text3);

        // Nii ei saa stringe võrrelda, kuna me tegelikult võrdleksime mälupiirkonna aadresse.
        System.out.println("== " + text3 == text4);

        // Stringe võrreldakes nii...
        System.out.println(".equals() " + text3.equals(text4));
        System.out.println(".equalsIgnoreCase() " + text3.equalsIgnoreCase(text4));

        String text5 = "Isa ütles:\n \"Tule siia!\"";
        System.out.println(text5);

        String text6 = "Minu salajane fail: C:\\secret\\document.txt";
        System.out.println(text6);
    }
}
