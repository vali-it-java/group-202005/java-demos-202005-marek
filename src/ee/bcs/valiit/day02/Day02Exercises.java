package ee.bcs.valiit.day02;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Day02Exercises {
    public static void main(String[] args) {
        // Ex 1
        // Defineeri muutuja, mis hoiaks väärtust 456.78.
        float myNum1 = 456.78F;
        double myNum2 = 456.78D;

        // Defineeri muutuja, mis viitaks tähtede jadale "test".
        String myText = "test";
        char[] myText2 = {'t', 'e', 's', 't'};

        // Defineeri muutuja, mis hoiaks tõeväärtust, mis viitaks avaldise tõesusele.
        boolean theAbsoluteTruth = !!!false;

        // Defineeri kaks erinevat tüüpi muutujat, mis hoiaksid endas väärtust 'a'.
        char myA = 'a';
        String myA2 = "a";

        // Defineeri muutuja, mis viitaks numbrite kogumile 5, 91, 304, 405.
        int[] myNums = {5, 91, 304, 405};

        // Defineeri muutuja, mis viitaks numbrite kogumile 56.7, 45.8, 91.2.
        double[] myDecimals = {56.7, 45.8, 91.2};

        Object[] myStuff = {"see on esimene väärtus", 67, 58.92};

        BigInteger myVeryLargeInteger =
                new BigInteger("7676868683452352345324534534523453245234523452345234523452345");

        BigDecimal myVeryLargeDecimal =
                new BigDecimal("7676868683452352345324534534523453245234523452345234523452345.345353445");
    }
}
