package ee.bcs.valiit.day02;

public class TextProcessingExercises {
    public static void main(String[] args) {

        System.out.println("Ex 1");
        System.out.println("Hello, World!");
        System.out.println("Hello, \"World\"!");
        String stevenQuote = "Steven Hawking once said: \"Life would be tragic if it weren't funny\".";
        System.out.println(stevenQuote);
        String complexSentence = "Kui liita kokku sõned \"See on teksti esimene pool  \" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\".";
        System.out.println(complexSentence);
        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        String mostBeautifulSentence = "Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"";
        System.out.println(mostBeautifulSentence);
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");

        System.out.println("Ex 2");
        String tallinnPopulation = "450 000";
        String populationText1 = "Tallinnas elab " + tallinnPopulation + " inimest.";
        System.out.println(populationText1);
        String populationText2 = String.format("Tallinnas elab %s inimest!", tallinnPopulation);
        System.out.println(populationText2);
        int populationOfTallinn​ = 450_000;
        String populationText3 = String.format("Tallinnas elab %,d inimest!!!", populationOfTallinn​);
        System.out.println(populationText3);

        System.out.println("Ex 3");
        String bookTitle = "Rehepapp";
        String bookText = String.format("Raamatu \"%s\" autor on Andrus Kivirähk.", bookTitle);
        System.out.println(bookText);
        String bookText2 = "Raamatu \"" + bookTitle + "\" autor on Andrus Kivirähk.";
        System.out.println(bookText2);

        System.out.println("Ex 4");
        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        int planetCount = 8;
        String planetText1 = planet1 + ", " + planet2 + ", " + planet3 + ", "
                + planet4 + ", " + planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 +
                " on Päikesesüsteemi " + planetCount + " planeeti.";
        System.out.println(planetText1);
        String planetText2 = String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount);
        System.out.println(planetText2);
    }
}
