package ee.bcs.valiit.day02;

public class Day02ConditionStructureExercises {

    public static void main(String[] args) {

        System.out.println("Ex 2");
        String city = args[0];
        System.out.println("city muutuja väärtus: " + city);
        if (city.equals("Milano")) {
            System.out.println("Ilm on soe!");
        } else {
            System.out.println("Ilm polegi kõige tähtsam!");
        }

        System.out.println("Ex 3");

        // String <--> number konversioon
        // 1) String --> int: Integer.parseInt("3") --> 3
        //    String --> double: Double.parseDouble("3.3") --> 3.3
        // 2) int --> String: String.valueOf(34.34)

        int grade = Integer.parseInt(args[1]);
        if (grade == 1) {
            System.out.println("nõrk");
        } else if (grade == 2) {
            System.out.println("mitterahuldav");
        } else if (grade == 3) {
            System.out.println("rahuldav");
        } else if (grade == 4) {
            System.out.println("hea");
        } else if (grade == 5) {
            System.out.println("suurepärane");
        } else {
            System.out.println("tundmatu väärtus");
        }

        System.out.println("Ex 4");
        switch (grade) {
            case 1:
                System.out.println("nõrk");
                break;
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("suurepärane");
                break;
            default:
                System.out.println("tundmatu väärtus");
        }

        System.out.println("Ex 5");
        String ageDescription;
        int age = 189;
        if (age > 0) {
            ageDescription = age > 100 ? "Vana" : "Noor";
            System.out.println(ageDescription);
        } else {
            System.out.println("Tundmatu vanus");
        }

        System.out.println("Ex 6");
        ageDescription = (age > 100) ?
                ("Vana")
                :
                (age == 100 ? "Peaaegu vana" : "Noor");
        System.out.println(ageDescription);

        if (age < 100) {
            ageDescription = "Noor";
        } else if (age == 100) {
            ageDescription = "Peaaegu vana";
        } else {
            ageDescription = "Vana";
        }
        System.out.println(ageDescription);
    }
}
