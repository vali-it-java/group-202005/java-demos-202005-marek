package ee.bcs.valiit.day02;

public class ExpressionsDemo {

    public static void main(String[] args) {

        // Arithmetic Expressions
        int a = 1;
        int b = 2;
        int c = a + b - 3 * b;
        System.out.println(c);

        a++;
        System.out.println(a);

        a--;
        System.out.println(a);

        int d = ++a;
        System.out.println(d);

        int e = 12;

//        e = e + 3;
        e += 3;

//        e = e - 3;
        e -= 3;

//        e = e * 3;
        e *= 3;
        System.out.println(e);

        int f = 80;
        f = f % 10;
        System.out.println(f);

        // Conditional Expressions
        // = omistamisoperaator
        // == võrdsuse kontrolli operaator
        // != mittevõrdsuse kontrolli operaator
        boolean isCorrect = 5 != 6;
        System.out.println(isCorrect);

//        Tõde ja Vale 	--> Vale
//        Tõde või Vale 	--> Tõde
//
//        1 - tõde
//        0 - vale
//                * - ja (Javas &&) kõrgema prioriteediga tehe
//                + - või (Java ||) madalama prioriteediga tehe
//
//        1 + 0 = 1
//        1 + 1 = 1
//        0 + 0 = 0
//
//        1 * 1 = 1
//        1 * 0 = 0
//        0 * 0 = 0
//
//        true || false && true = true
//        1    +  0     *  1    = 1
    }
}
