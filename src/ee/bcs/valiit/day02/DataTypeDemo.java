package ee.bcs.valiit.day02;

public class DataTypeDemo {
    public static void main(String[] args) {

        // See on kommentaar, mida kompilaator ei näe.
        /*
            Mulle tundub, et
            kompilaator ignoreerib mind.
         */

        // Andmetüübid

        // 1 - byte (1 bait)
        int myNumber = 12;
        byte mySmallNumber = (byte)211;
        mySmallNumber = (byte)myNumber;
        mySmallNumber = 98;
        System.out.println(mySmallNumber);

        // 2 - short (2 baiti)
        short myMediumSizedNumber = 11000;
        System.out.println(myMediumSizedNumber);

        // 3 - int (4 baiti)
        int myRelativelyLargeNumber = 1_000_000_000;
        System.out.println(myRelativelyLargeNumber);

        // 4 - long (8 baiti)
        long myVeryLargeNumber = 200_000_000_000_000_000L;
        System.out.println(myVeryLargeNumber);

//        myRelativelyLargeNumber = (int)myVeryLargeNumber;
//        System.out.println(myRelativelyLargeNumber);

        // 5 - float (4 baiti)
        float myDecimal1 = 4.8F;
        float myDecimal2 = 56.4F;
        float myDecimal3 = myDecimal1 + myDecimal2;
        System.out.println(myDecimal3);

        // 6 - double (8 baiti)
        double myDecimal4 = 4.8;
        double myDecimal5 = 56.4D;
        double myDecimal6 = myDecimal4 + myDecimal5;
        System.out.println(myDecimal6);

        // 7 - char (kaks baiti, nagu short)
        char myChar1 = 's';
        short myChar3 = 116;
        System.out.println((char)myChar3);

        char myChar2 = '2';
        System.out.println((short)myChar2);

        // 8 - boolean (1 bait)
        boolean isEarthRound = true || false && true;
        System.out.println(isEarthRound);

        // Wrapper classes
        Long wrapperA = 5L;
        Long wrapperB = 8L;
        long wrapersSum = wrapperA + wrapperB;
        System.out.println(wrapersSum);
    }
}
