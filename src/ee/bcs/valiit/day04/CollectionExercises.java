package ee.bcs.valiit.day04;

import java.util.*;
import java.util.stream.Stream;

public class CollectionExercises {

    public static void main(String[] args) {

        System.out.println("Ex 19");
//        List<String> cities = new ArrayList<>();
//        cities.add("Berlin");
//        cities.add("New York");
//        cities.add("Moskva");
//        cities.add("Riia");
//        cities.add("Tapa");
        List<String> cities = new ArrayList<>(Arrays.asList("Berlin", "New York", "Moskva", "Riia", "Tapa"));
        cities.add("Baden-Baden");
        System.out.println("Esimene linn: " + cities.get(0));
        System.out.println("Kolmas linn: " + cities.get(2));
        System.out.println("Viimane linn: " + cities.get(cities.size() - 1));

        System.out.println("Ex 20");
        Queue<String> namesQueue = new LinkedList<>();
        namesQueue.add("Mari");
        namesQueue.add("Teet");
        namesQueue.add("Toomas");
        namesQueue.add("Adalbert");
        namesQueue.add("Kairi");
        namesQueue.add("Nele");
        while (!namesQueue.isEmpty()) {
            System.out.println(namesQueue.remove());
        }

        System.out.println("Ex 21");
        Set<String> namesSet = new TreeSet<>();
        namesSet.add("Mari");
        namesSet.add("Teet");
        namesSet.add("Toomas");
        namesSet.add("Adalbert");
        namesSet.add("Kairi");
        namesSet.add("Nele");
//        namesSet.forEach(n -> System.out.println(n));
        namesSet.forEach(System.out::println);

        System.out.println("Ex 22");
        Map<String, String[]> countries = new HashMap<>();
        String[] estoniaCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        countries.put("Estonia", estoniaCities);
        String[] swedenCities = {"Stockholm", "Uppsala", "Lund", "Köping"};
        countries.put("Sweden", swedenCities);
        countries.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});
        System.out.println(countries);

        Map<String, List<String>> countries2 = new HashMap<>();
        countries2.put("Estonia", Arrays.asList("Tallinn", "Tartu", "Valga", "Võru"));
        countries2.put("Sweden", Arrays.asList("Stockholm", "Uppsala", "Lund", "Köping"));
        countries2.put("Finland", Arrays.asList("Helsinki", "Espoo", "Hanko", "Jämsä"));
        System.out.println(countries2);

//        for (String country : countries.keySet()) {
//            System.out.println("Country: " + country);
//            System.out.println("Cities:");
//            for (String city : countries.get(country)) {
//                System.out.println("\t" + city);
//            }
//        }

        countries.forEach((country, countryCities) -> {
            System.out.println("Country: " + country);
            System.out.println("Cities:");
            Arrays.asList(countryCities).stream().forEach(countryCity -> System.out.println("\t\t" + countryCity));
//            Stream.of(countryCities).forEach(countryCity -> System.out.println("\t\t" + countryCity));
        });

    }
}
