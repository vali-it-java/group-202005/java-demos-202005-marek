package ee.bcs.valiit.day04;

import java.util.Arrays;

public class LoopDemo {

    public static void main(String[] args) {

        // for-tsükkel

//        System.out.println("Tsükli algus...");
//        int i = 0;
//        for (;i < 6; i++) {
//            System.out.println("Tsükli kord: " + i);
//        }
//        System.out.println("Tsükli lõpp: " + i);

//        // Igavene tsükkel: tee mittemidagi hästi intensiivselt.
//        for(;;) {}

        String[] suits = { "Ruutu", "Risti", "Ärtu", "Poti" };
//        System.out.println(Arrays.toString(suits));
//        for (int t = 0; t < suits.length; t++) {
//            System.out.println("Mast: " + suits[t]);
//        }

        for (int t = suits.length - 1; t >= 0; t--) {
            System.out.println("Mast: " + suits[t]);
        }

        // foreach-tsükkel

        for (String suit : suits) {
            System.out.println("Mast (foreach meetodil): " + suit);
        }

        // while-tsükkel

//        while(true) {
//            // Palun sisesta number
//            // Kui kasutaja ei sisesta numbrit (sisestab tähe), siis alustame otsast peale.
//            // Kui kasutaja sisestab korrektse numbri, siis lõpetame tsükli.
//            // Kuidas lõpetada tsükkel?
//            if (korrektneArv) {
//                break; // lõpetab tsükli
//            }
//        }

        int suitIndex = 0;
        while (suitIndex < suits.length) {
            System.out.println("Mast (while tsükliga): " + suits[suitIndex]);
            suitIndex++;
        }

        // do..while-tsükkel
        // Enne tee ja siis mõtle!

        int suitIndex2 = 0;
        do {
            System.out.println("Mast (do..while tsükliga): " + suits[suitIndex2]);
            suitIndex2++;
        } while (suitIndex2 < suits.length);

    }
}
