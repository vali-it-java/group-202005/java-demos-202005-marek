package ee.bcs.valiit.day03;

import java.util.Arrays;

public class ArrayExercises {

    public static void main(String[] args) {

        System.out.println("Ex 7");
        int[] myNumbers;
        myNumbers = new int[5];
        myNumbers[0] = 1;
        myNumbers[1] = 2;
        myNumbers[2] = 3;
        myNumbers[3] = 4;
        myNumbers[4] = 5;
        System.out.println("Esimene element: " + myNumbers[0]);
        System.out.println("Kolmas element: " + myNumbers[2]);
        System.out.println("Viimane element: " + myNumbers[myNumbers.length - 1]);

        System.out.println("Ex 8");
        // Inline meetodil
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};

        // Klassikaliselt
        String[] cities2 = new String[4];
        cities2[0] = "Tallinn";
        cities2[1] = "Helsinki";
        cities2[2] = "Madrid";
        cities2[3] = "Paris";
        System.out.println(Arrays.toString(cities2));

        // Palun ärge neid nimesid kasutage klassi loomisel: Object, String, Test, Array, Arrays...

        System.out.println("Ex 9");
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9, 0}};

        int[][] matrix2 = new int[3][];
        matrix2[0] = new int[3];
        matrix2[0][0] = 1;
        matrix2[0][1] = 2;
        matrix2[0][2] = 3;
        matrix2[1] = new int[3];
        matrix2[1][0] = 4;
        matrix2[1][1] = 5;
        matrix2[1][2] = 6;
        matrix2[2] = new int[]{7, 8, 9, 0};

        System.out.println("Ex 10");
        String[][] countryCities = {
                {"Tallinn", "Tartu", "Valga", "Võru", "Kohila"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
        };
        String[][] countryCities2 = new String[3][4];
        countryCities2[0] = new String[5];
        countryCities2[0][0] = "Tallinn";
        countryCities2[0][1] = "Tartu";
        countryCities2[0][2] = "Valga";
        countryCities2[0][3] = "Võru";
        countryCities2[0][4] = "Kohila";
        countryCities2[1][0] = "Stockholm";
        countryCities2[1][1] = "Uppsala";
        countryCities2[1][2] = "Lund";
        countryCities2[1][3] = "Köping";
        countryCities2[2][0] = "Helsinki";
        countryCities2[2][1] = "Espoo";
        countryCities2[2][2] = "Hanko";
        countryCities2[2][3] = "Jämsä";


    }
}
