package ee.bcs.valiit.day03;

public class Branching {

    public static void main(String[] args) {

        boolean isValid = true;
        if (isValid) {

        }
//        else if (anotherValueIsValid) {
//
//        } else if (anotherValueIsValid2) {
//
//        } else if (anotherValueIsValid3) {
//
//        } else {
//
//        }


        int someNumber = 9;

        // Tingimustehe (inline-if statement)
        String canDivideByThree = someNumber % 3 == 0 ? "jagub kolmega" : "ei jagu kolmega";
        System.out.println(canDivideByThree);

        // klassikaline if-else statement
        if (someNumber % 3 == 0) {
            canDivideByThree = "jagub kolmega";
        } else {
            canDivideByThree = "ei jagu kolmega";
        }
        System.out.println(canDivideByThree);

        // Switch statement
        // Kasulik kasutada siis, kui
        // a) kui valikuvariante on kindel kogus
        // b) kui hargnemisi on palju
        String trafficLightColor = "green";

        switch(trafficLightColor) {
            case "red":
                System.out.println("Cars must stop and wait.");
                // Another statements...
                break;
            case "yellow":
                System.out.println("Cars must slow down...");
                break;
            case "green":
                System.out.println("Cars can drive normally.");
                break;
            default:
                System.out.println("Help! Traffic lights are broken!");
        }
    }
}
