package ee.bcs.valiit.day03;

import java.util.Scanner;

public class TextProcessing2 {
    // Konstandi defineerimine
    public static final double VALUE_ADDED_TAX_RATE = 1.2;

    // VALUE_ADDED_TAX_RATE - convention name: Screaming Snake Case
    // value_added_tax_rate - snake case

    public static void main(String[] args) {

        // Muutumatu muutuja ehk konstant
        final String a = "Hello!";
        final int b = 5;
        // teen muutujaga a asju...

        // See operatsioon ei õnnestu, kuna a on "final".
//        a = "Good Bye!";
//        b = 6;
//        VALUE_ADDED_TAX_RATE = 1.3;

        String myStr1 = "aaa";
        String myStr2 = "bbb";
        myStr1 = myStr1 + myStr2; // Tekitatakse täiesti uus stringi objekt.
        // Sellised operatsioonid tekitavad jõudlusprobleeme, kui
        // manipuleerime väga suuri stringe.

        // Lahendus...
        // StringBuilder või StringBuffer (jõudluse parandajad)
        StringBuilder sb = new StringBuilder();
        sb.append("aaa");
        sb.append("bbb");
        // ...
        System.out.println(sb.toString());

        // Scanner...
        String names = "Mari, Malle, Mart, Meelis";
        Scanner myScanner = new Scanner(names);
        myScanner.useDelimiter(", ");
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
    }
}
