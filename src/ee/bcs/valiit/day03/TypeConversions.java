package ee.bcs.valiit.day03;

public class TypeConversions {
    public static void main(String[] args) {

        // Primitive --> String conversions
        int numberA = 123;
        String textA = String.valueOf(numberA);
        System.out.println(textA);

        boolean boolB = true;
        String textB = String.valueOf(boolB);
        System.out.println(textB);

        // String --> Primitive conversion
        String textC = "Nipitiri";
        boolean boolC = Boolean.parseBoolean(textC);
        System.out.println(boolC);

        String textD = "123456";
        int numberD = Integer.parseInt(textD);
        System.out.println(numberD);


        int i = 777; // 00000000 00000000 00000011 00001001
        byte b = ((Integer)i).byteValue(); // 00001001 == 9 DEC
//        byte b = (byte)i; // 00001001 == 9 DEC
        System.out.println(b);

        double d = 123.456;
//        String textDouble = ((Double)d).toString();
        String textDouble = String.valueOf(d);
        System.out.println(textDouble);
    }
}
