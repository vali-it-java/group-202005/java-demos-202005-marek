package ee.bcs.valiit.day03;

import java.util.Scanner;

public class TextProcessingExercises2 {
    public static void main(String[] args) {
        System.out.println("Ex 1");
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";
        StringBuilder stringBulder = new StringBuilder();
        stringBulder.append(president1);
        stringBulder.append(", ");
        stringBulder.append(president2);
        stringBulder.append(", ");
        stringBulder.append(president3);
        stringBulder.append(", ");
        stringBulder.append(president4);
        stringBulder.append(" ja ");
        stringBulder.append(president5);
        stringBulder.append(" on Eesti presidendid.");
        System.out.println(stringBulder);

        System.out.println("Ex 2");
        String text = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
        Scanner myScanner = new Scanner(text);
        myScanner.useDelimiter("Rida: ");
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
    }
}
