package ee.bcs.valiit.pranglija;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PranglijaApp {
    private static final int GUESS_COUNT = 3;
    private static List<GameResult> gameLog = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("------------------------------------");
        System.out.println("PRANGLIJA ÄPP");
        System.out.println("------------------------------------");

        while(true) {
            // Mäng...
            System.out.print("Sisesta oma nimi: ");
            String playerName = scanner.nextLine();

            System.out.print("Sisesta numbrivahemiku miinimum: ");
            int rangeMin = getIntFromConsole();

            System.out.print("Sisesta numbrivahemiku maksimum: ");
            int rangeMax = getIntFromConsole();

            // 01.01.1970 00:00:00 - ajaarvamise algus.
            long startTime = System.currentTimeMillis();

            // Must maagia
            int count = 0;
            for (; count < GUESS_COUNT; count++) {
                int number1 = (int) (Math.random() * (rangeMax - rangeMin + 1)) + rangeMin;
                int number2 = (int) (Math.random() * (rangeMax - rangeMin + 1)) + rangeMin;
                System.out.print(String.format("%d + %d = ", number1, number2));
                int userGuess = Integer.parseInt(scanner.nextLine());
                if (number1 + number2 != userGuess) {
                    // Kasutaja eksis liitmisel...
                    break;
                }
            }

            if (count == GUESS_COUNT) {
                System.out.println("Tubli! Sa võitsid!");

                long endTime = System.currentTimeMillis();
                double elapsedTime = endTime - startTime;
                elapsedTime = elapsedTime / 1000.0;
                System.out.println("Mäng kestis " + elapsedTime + " sekundit.");

                // Logime mängu gameLog muutujasse...
                // Tekitame objekti mängu tulemuse hoidmiseks (instruktsiooniks GameResult)
                GameResult result = new GameResult();
                result.name = playerName;
                result.time = elapsedTime;
                result.numberRange = rangeMin + " - " + rangeMax;
                gameLog.add(result);
                gameLog.sort((resultA, resultB) -> {
                    if (resultA.time > resultB.time) {
                        return 1; // indikatsioon, vaheta resultA ja resultB järjekord ära
                    } else {
                        return -1; // järjekord juba ongi õige, ära muuda resultA ja resultB järjekorda
                    }
                });

                // Kuvame tulemuse...
                System.out.println("------------------------------------");
                System.out.println("MÄNGU LOGI");
                for (GameResult gameResult : gameLog) {
                    System.out.println("------------------------------------");
                    System.out.println("Nimi:\t\t\t" + gameResult.name);
                    System.out.println("Aeg:\t\t\t" + gameResult.time + " sekundit");
                    System.out.println("Numbrivahemik:\t" + gameResult.numberRange);
                    System.out.println();
                }
                System.out.println("------------------------------------");
            } else {
                System.out.println("Paha lugu :(");
            }

            System.out.println("Mäng on lõppenud. Mida soovid edasi teha?");
            int userCommand = getIntFromConsole();
            if (userCommand == 0) {
                break;
            }
        }

        System.out.println("------------------------------------");
        System.out.println("Copyright: Marek Lints (all rights reserved)");
        System.out.println("------------------------------------");
    }

    private static int getIntFromConsole() {
        while(true) {
            try {
                return Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Sisestatud tekst ei ole number. Palun proovi uuesti!");
                System.out.print("Sisesta number: ");
            }
        }
    }
}
