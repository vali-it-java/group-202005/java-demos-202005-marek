package ee.bcs.valiit.pranglija;

public class GameResult {
    protected String name;
    public double time;
    public String numberRange;

    @Override
    public String toString() {
        return "GameResult{" +
                "name='" + name + '\'' +
                ", time=" + time +
                ", numberRange='" + numberRange + '\'' +
                '}';
    }
}
