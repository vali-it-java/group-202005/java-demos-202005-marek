package ee.bcs.valiit.day05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdditionalExercises {
    public static void main(String[] args) {
        System.out.println("Ex 1 pattern 1");
        for (int row = 1; row <= 8; row++) {
            for (int col = 1; col <= 19; col++) {
                System.out.print((col + row) % 2 == 0 ? "#" : "+");
//                if (row % 2 == 1) {
//                    System.out.print(col % 2 == 1 ? "#" : "+");
//                } else {
//                    System.out.print(col % 2 == 1 ? "+" : "#");
//                }
            }
            System.out.println();
        }

        System.out.println("Ex 1 pattern 2");
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j < i; j++) {
                System.out.print("-");
            }
            System.out.println("#");
        }

        System.out.println("Ex 1 pattern 3");
        for (int row = 1; row <= 5; row++) {
            for (int col = 1; col <= 7; col++) {
                if (row % 2 == 0) {
                    System.out.print("=");
                } else {
                    System.out.print(col % 3 == 1 ? "#" : "+");
                }
            }
            System.out.println();
        }

        System.out.println("Ex 2");
        String personsString = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA";

        System.out.println("Array demo:");
        String[] personsArray = personsString.split("; ");

        String[][] personDataArray = new String[personsArray.length][5];
        for(int i = 0; i < personsArray.length; i++) {
            String[] personArray = personsArray[i].split(", ");
            for (int j = 0; j < personArray.length; j++) {
                personArray[j] = personArray[j].split(": ")[1];
            }
            personDataArray[i] = personArray;
        }

        for (String[] person : personDataArray) {
            System.out.println("----------------------------");
            System.out.printf("%s / %s / %s aastat / ametilt %s / kodakondsus %s \n", person[0], person[1], person[2], person[3], person[4]);
        }

        // Näide lõpptulemusest (String[][])
//        {
//            {"Teet", "Kask", "34", "lendur", "Eesti"},
//            {"Mari", "Tamm", ..}
//        }

        System.out.println("List demo:");
        List<List<String>> personDataList = new ArrayList<>();
        for(String personLine : personsString.split("; ")) {
            List<String> personData = new ArrayList<>();
            for(String personProperty : personLine.split(", ")) {
                personData.add(personProperty.split(": ")[1]);
            }
            personDataList.add(personData);
        }
//        System.out.println(personDataList);

        for (List<String> person : personDataList) {
            System.out.println("----------------------------");
            System.out.printf("%s / %s / %s aastat / ametilt %s / kodakondsus %s \n",
                    person.get(0), person.get(1), person.get(2), person.get(3), person.get(4));
        }

        System.out.println("Map demo:");
        // Map: "Teet Kask" --> ["34", "lendur", "Eesti"], ...

        Map<String, List<String>> personDataMap = new HashMap<>();
        for(String personLine : personsString.split("; ")) {
            String[] personPropArray = personLine.split(", ");
            for (int i = 0; i < personPropArray.length; i++) {
                personPropArray[i] = personPropArray[i].split(": ")[1];
            }

            String key = personPropArray[0] + " " + personPropArray[1];
            List<String> personData = new ArrayList<>();
            for(int i = 2; i < personDataArray.length; i++) {
                personData.add(personPropArray[i]);
            }
            personDataMap.put(key, personData);
        }
//        System.out.println(personDataMap);
        for (String personFullName : personDataMap.keySet()) {
            List<String> person = personDataMap.get(personFullName);
            System.out.println("----------------------------");
            System.out.printf("%s / %s aastat / ametilt %s / kodakondsus %s \n",
                    personFullName, person.get(0), person.get(1), person.get(2));
        }
    }
}
