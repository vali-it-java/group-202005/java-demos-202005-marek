package ee.bcs.valiit.day05;

import org.w3c.dom.ls.LSOutput;

import java.util.*;

public class Exercises {

    public static void main(String[] args) {

        System.out.println("Ex 1");
        // Kahe teineteise sees oleva for-tsükli ülesanne.
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= 6; col++) {
//                Rida 1: 6 (7 - rida)
//                Rida 2: 5 (7 - rida)
//                Rida 3: 4 (7 - rida)
//                Rida 4: 3 (7 - rida)
//                Rida 5: 2 (7 - rida)
//                Rida 6: 1 (7 - rida)
                if (col <= 7 - row) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        System.out.println("Ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }

        System.out.println("Ex 3");
        // Variant 1
//        for (int row = 1; row <= 5; row++) {
//            // Esimene kolmnurk
//            for (int col = 1; col <= 5 - row; col++) {
//                System.out.print(" ");
//            }
//            // Teine kolmnurk
//            for (int col = 1; col <= row; col++) {
//                System.out.print("@");
//            }
//            System.out.println();
//        }

        // Variant 2
        for (int row = 1; row <= 5; row++) {
            for (int col = 1; col <= 5; col++) {
//                if (col <= 5 - row) {
//                    System.out.print(" ");
//                } else {
//                    System.out.print("@");
//                }
                System.out.print(col <= 5 - row ? " " : "@");
            }
            System.out.println();
        }

        System.out.println("Ex 4");
        // Variant 1: for-tsükkel
        int number = 1234567;
        String numberString = String.valueOf(number); // 1234567 -> "1234567"
        String reversedNumberString = "";
        for (char c : numberString.toCharArray()) {
            reversedNumberString = c + reversedNumberString;
        }
        int result = Integer.parseInt(reversedNumberString); // "7654321" -> 7654321
        System.out.println(result);

        // Variant 2: StringBulder
        number = 389;
        numberString = String.valueOf(number);
        StringBuilder stringBuilder = new StringBuilder(numberString);
        reversedNumberString = stringBuilder.reverse().toString();
        result = Integer.parseInt(reversedNumberString);
        System.out.println(result);

        System.out.println("Ex 5");
        for (int i = 0; i < args.length; i += 2) {
            // args{"Marek", "89"} --> "Marek: PASS - 4, 89"
            // args{"Daniil", "43"} --> "Daniil: FAIL"

            String studentName = args[i];
            int studentPoints = Integer.parseInt(args[i + 1]);
            int grade = 0;

            if (studentPoints < 51) {
                grade = 0;
            } else if (studentPoints < 61) {
                grade = 1;
            } else if (studentPoints < 71) {
                grade = 2;
            } else if (studentPoints < 81) {
                grade = 3;
            } else if (studentPoints < 91) {
                grade = 4;
            } else if (studentPoints < 101) {
                grade = 5;
            } else {
                grade = -1;
            }

            if (grade == 0) {
                System.out.println(String.format("%s: FAIL", studentName));
            } else if (grade > 0) {
                System.out.println(String.format("%s: PASS - %d, %d", studentName, grade, studentPoints));
            } else {
                System.out.println("Viga: ebakorrektne sisend!");
            }
        }

        System.out.println("Ex 6");
        double[][] triangleSideSet = {
                {56.91, 45.39},
                {23.65, 12.11},
                {98.99, 78.65},
                {123.61, 896.23},
                {456.172, 654.546},
                {349.0, 672.7},
                {3, 4}
        };

        for (int i = 0; i < triangleSideSet.length; i++) {
            double sideA = triangleSideSet[i][0];
            double sideB = triangleSideSet[i][1];
            double sideC = Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
            System.out.println(String.format("Täisnurkne kolmnurk küljepikkustega %.2f ja %.2f omab hüpotenuusi pikkusega %.2f.",
                    sideA, sideB, sideC));
        }

        System.out.println("Ex 7");
        // Estonia, Tallinn, Jüri Ratas
        // Latvia, Riga, Arturs Krišjānis Kariņš
        // Lithuania, Vilnius, Saulius Skvernelis
        // Finland, Helsinki, Sanna Marin
        // Norway, Oslo, Erna Solberg
        // Denmark, Copenhagen, Mette Frederiksen
        // Russia, Moscow, Mikhail Mishustin
        // Germany, Berlin, Angela Merkel
        // France, Paris, Édouard Philippe
        // Sweden, Stockholm, Stefan Löfven
        String[][] countries = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
                {"Finland", "Helsinki", "Sanna Marin"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"},
                {"Russia", "Moscow", "Mikhail Mishustin"},
                {"Germany", "Berlin", "Angela Merkel"},
                {"France", "Paris", "Édouard Philippe"},
                {"Sweden", "Stockholm", "Stefan Löfven"}
        };
        // Variant 1: for-tsükkel
        for (int i = 0; i < countries.length; i++) {
            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s",
                    countries[i][0], countries[i][1], countries[i][2]));
        }

        // Variant 2: foreach-tsükkel
//        for (String[] country : countries) {
//            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s",
//                    country[0], country[1], country[2]));
//        }

        System.out.println("Ex 8");
        String[][][] countries2 = {
                {{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}},
                {{"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Estonian"}},
                {{"Lithuania"}, {"Vilnius"}, {"Saulius Skvernelis"}, {"Lithuanian", "Polish", "Russian"}},
                {{"Finland"}, {"Helsinki"}, {"Sanna Marin"}, {"Finnish", "Swedish", "Sami", "Estonian"}},
                {{"Norway"}, {"Oslo"}, {"Erna Solberg"}, {"Norwegian", "Swedish", "English"}},
                {{"Denmark"}, {"Copenhagen"}, {"Mette Frederiksen"}, {"Danish", "Swedish", "German"}},
                {{"Russia"}, {"Moscow"}, {"Mikhail Mishustin"}, {"Russian", "English"}},
                {{"Germany"}, {"Berlin"}, {"Angela Merkel"}, {"German", "French", "Polish", "English"}},
                {{"France"}, {"Paris"}, {"Édouard Philippe"}, {"French"}},
                {{"Sweden"}, {"Stockholm"}, {"Stefan Löfven"}, {"Swedish", "Danish", "English", "German"}}
        };

        for (String[][] country : countries2) {
            String countryName = country[0][0];
            String countryCapital = country[1][0];
            String countryPrimeMinister = country[2][0];
            String[] countryLanguages = country[3];

            System.out.println(String.format("%s / %s / %s:", countryName, countryCapital, countryPrimeMinister));
            for (String countryLanguage : countryLanguages) {
                System.out.println("\t\t" + countryLanguage);
            }
        }

        System.out.println("Ex 9");
        List<List<List<String>>> countries3 =
                Arrays.asList( // riigid
                        Arrays.asList( // riik
                                Collections.singletonList("Estonia"), // riigi nimi
                                Collections.singletonList("Tallinn"), // riigi pealinn
                                Collections.singletonList("Jüri Ratas"), // riigi peaminister
                                Arrays.asList("Estonian", "Russian", "Finnish") // riigi keeled
                        ),
                        Arrays.asList(
                                Collections.singletonList("Latvia"),
                                Collections.singletonList("Riga"),
                                Collections.singletonList("Arturs Krišjānis Kariņš"),
                                Arrays.asList("Latvian", "Russian", "Estonian")
                        ),
                        Arrays.asList(
                                Collections.singletonList("Lithuania"),
                                Collections.singletonList("Vilnius"),
                                Collections.singletonList("Saulius Skvernelis"),
                                Arrays.asList("Lithuanian", "Polish", "Russian")
                        )
                );

        for (int i = 0; i < countries3.size(); i++) {
            String countryName = countries3.get(i).get(0).get(0);
            String countryCapital = countries3.get(i).get(1).get(0);
            String countryPrimeMinister = countries3.get(i).get(2).get(0);
            List<String> countryLanguages = countries3.get(i).get(3);

//            System.out.println(String.format());
            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
            for (int j = 0; j < countryLanguages.size(); j++) {
                System.out.println("\t\t" + countryLanguages.get(j));
            }
        }

        System.out.println("Ex 10");
        Map<String, String[][]> countries4 = new TreeMap<>();
        countries4.put(
                "Estonia",
                new String[][]{{"Tallinn", "Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}}
        );
        countries4.put(
                "Sweden",
                new String[][]{{"Stockholm", "Stefan Löfven"}, {"Swedish", "Danish", "German", "English"}}
        );
        countries4.put(
                "Norway",
                new String[][]{{"Oslo", "Erna Solberg"}, {"Norwegian", "Swedish", "English"}}
        );

        for (String countryName : countries4.keySet()) {
            String countryCapital = countries4.get(countryName)[0][0];
            String countryPrimeMinister = countries4.get(countryName)[0][1];
            String[] countryLanguages = countries4.get(countryName)[1];

            System.out.println(String.format("%s / %s / %s:", countryName, countryCapital, countryPrimeMinister));
            for (String countryLanguage : countryLanguages) {
                System.out.println("\t\t" + countryLanguage);
            }
        }

        System.out.println("Ex 11");
        // Queue --> LinkedList
        Queue<String[][]> countries5 = new LinkedList<>();
        countries5.add(new String[][]{{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}});
        countries5.add(new String[][]{{"Germany"}, {"Berlin"}, {"Angela Merkel"}, {"German", "French", "Polish", "English"}});
        countries5.add(new String[][]{{"Finland"}, {"Helsinki"}, {"Sanna Marin"}, {"Finnish", "Swedish", "Sami", "Estonian"}});

//        while (!countries5.isEmpty()) {
//            String[][] country = countries5.poll();
//            String countryName = country[0][0];
//            String countryCapital = country[1][0];
//            String countryPrimeMinister = country[2][0];
//            String[] countryLanguages = country[3];
//            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
//            for (String countryLanguage : countryLanguages) {
//                System.out.println("\t" + countryLanguage);
//            }
//        }

//        for (String[][] country : countries5){
//            String countryName = country[0][0];
//            String countryCapital = country[1][0];
//            String countryPrimeMinister = country[2][0];
//            String[] countryLanguages = country[3];
//            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
//            for (String countryLanguage : countryLanguages) {
//                System.out.println("\t" + countryLanguage);
//            }
//        }

        for (String[][] country : countries5.toArray(String[][][]::new)) {
                        String countryName = country[0][0];
            String countryCapital = country[1][0];
            String countryPrimeMinister = country[2][0];
            String[] countryLanguages = country[3];
            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
            for (String countryLanguage : countryLanguages) {
                System.out.println("\t" + countryLanguage);
            }
        }
    }
}
