package ee.bcs.valiit.day09;

import java.util.Objects;

public class Employee implements Comparable<Employee> {
    private String firstName;
    private String lastName;
    private int salary;
    private String nationality;

    public Employee(String firstName, String lastName, int salary, String nationality) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.nationality = nationality;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return  Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", salary=" + salary +
                ", nationality='" + nationality + '\'' +
                '}';
    }

    @Override
    public int compareTo(Employee another) {
        // Kui mina pean olema eespool, kui another, siis tagastan negatiivse arvu (-1, -567, -23987234)
        // Kui mina pean olema tagapool, kui another, siis tagastan positiivse arvu (positiivne ==> vaheta järjekord)
        // Kui mina olen võrdne anotheriga, siis tagasta 0
//        return this.firstName.compareTo(another.firstName) * -1;

        // Sorteerime palgataseme järgi kasvavalt...
//        if(this.salary > another.salary) {
//            return 1;
//        } else if (this.salary < another.salary) {
//            return -1;
//        } else {
//            return 0;
//        }
//        return this.salary - another.salary;

        // Sorteerime palgataseme, eesnime ja perenime järgi kasvavalt...
        if (this.salary - another.salary != 0) {
            return this.salary - another.salary;
        } else if (this.firstName.compareTo(another.firstName) != 0) {
            return this.firstName.compareTo(another.firstName);
        } else {
            return this.lastName.compareTo(another.lastName);
        }
    }
}
