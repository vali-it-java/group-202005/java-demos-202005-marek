package ee.bcs.valiit.day09;

import java.util.Comparator;

public class EmployeeSortingInstruction implements Comparator<Employee> {

    @Override
    public int compare(Employee employee1, Employee employee2) {
//        if (employee2.getSalary() - employee1.getSalary() != 0) {
//            return employee2.getSalary() - employee1.getSalary();
//        } else if (employee1.getFirstName().compareTo(employee2.getFirstName()) != 0) {
//            return employee1.getFirstName().compareTo(employee2.getFirstName());
//        } else {
//            return employee1.getLastName().compareTo(employee2.getLastName());
//        }
        return employee1.getSalary() - employee2.getSalary();
    }
}
