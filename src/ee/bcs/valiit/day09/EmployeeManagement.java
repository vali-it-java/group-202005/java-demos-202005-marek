package ee.bcs.valiit.day09;

import java.util.*;

public class EmployeeManagement {

    public static void main(String[] args) {
        Employee mati = new Employee("Mati", "Kask", 1500, "Eestlane");
        Employee kati = new Employee("Kati", "Tamm", 1800, "Rootslane");
        Employee priit = new Employee("Priit", "Järv", 1300, "Soomlane");
        Employee adalbert1 = new Employee("Adalbert", "Mägi", 1900, "Lätlane");
        Employee adalbert2 = new Employee("Adalbert", "Mägi", 1100, "Lätlane");

//        if (adalbert1.getFirstName().equals(adalbert2.getFirstName()) &&
//                (adalbert1.getLastName().equals(adalbert2.getLastName())) {
//            System.out.println("Adalberdid on võrdsed!");
//        }

        //System.out.println(adalbert1.equals(adalbert2));
//        Set<Employee> employeesSet = new HashSet<>();
//        employeesSet.add(mati);
//        employeesSet.add(kati);
//        employeesSet.add(priit);
//        employeesSet.add(adalbert1);
//        employeesSet.add(adalbert2);
//        System.out.println(employeesSet);

        List<Employee> employees = new ArrayList<>();
        employees.add(mati);
        employees.add(kati);
        employees.add(priit);
        employees.add(adalbert1);
        employees.add(adalbert2);
        // Variant 1: klassikaline lahendus
//        Collections.sort(employees, new EmployeeSortingInstruction());

        // Variant 2: inteface inline-implementatsioon (pool-modernne)
//        Comparator<Employee> empComparator = new Comparator<Employee>() {
//            @Override
//            public int compare(Employee employee1, Employee employee2) {
//                return employee2.getSalary() - employee1.getSalary();
//            }
//        };
//        Collections.sort(employees, empComparator);

        // Variant 3: Cool, äge, lahe, modernne! (lambda-avaldis)
        Collections.sort(employees, (employee1, employee2) -> employee1.getSalary() - employee2.getSalary());

        // Lambda avaldis on interface'i inline-realiseering kompaktses vormingus.
        // Lambda avaldist saab kasutada ainult siis, kui interface on @FunctionalInterface
        // Mis on functional interface - see on interface, millel on ainult üks implementeerimist vajav meetod.

        // Variant 4: Super cool!
//        Collections.sort(employees, Comparator.comparingInt(Employee::getSalary));

        System.out.println(employees);
    }
}
