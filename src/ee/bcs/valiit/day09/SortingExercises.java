package ee.bcs.valiit.day09;

import ee.bcs.valiit.bank.Account;
import ee.bcs.valiit.bank.AccountService;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SortingExercises {

    public static void main(String[] args) throws IOException {

        AccountService.loadAccounts("resources/kontod.txt");
        List<Account> customers = AccountService.getAccounts();

        System.out.println("Sorteerime konto jäägi järgi kahanevalt...");
        // Variant 1:
//        Collections.sort(customers, (a1, a2) -> (int) (a2.getBalance() - a1.getBalance()));
        // Variant 2:
        customers.sort((a1, a2) -> (int) (a2.getBalance() - a1.getBalance()));
        System.out.println(customers);

        System.out.println("Sorteerime eesnime ja perenime järgi...");
        Collections.sort(customers, (a1, a2) -> {
            if (a1.getFirstName().compareTo(a2.getFirstName()) != 0) {
                return a1.getFirstName().compareTo(a2.getFirstName());
            } else {
                return a1.getLastName().compareTo(a2.getLastName());
            }
        });
        System.out.println(customers);

        System.out.println("Otsime (filtreerime) välja rikkad kliendid (rikastel on üle 1000 euro). Sorteerime konto jäägi järgi kasvavalt...");
        List<Account> richCustomers = customers.stream().filter(a -> a.getBalance() > 1000).collect(Collectors.toList());
        richCustomers.sort((a1, a2) -> (int)(a1.getBalance() - a2.getBalance()) );
        System.out.println(richCustomers);
    }
}
